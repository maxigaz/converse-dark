# Converse Dark Theme

A dark theme for the full page version of [Converse](https://conversejs.org/), a web-based XMPP client.

This style is written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## Install with Stylus

If you have Stylus installed, click on the banner below and a new window of Stylus should open, asking you to confirm to install the style.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/converse-dark/raw/master/converse-dark.user.css)

## Screenshots

![A screenshot showing a chat room of a one-to-one conversation](screenshot1.png)

![A screenshot showing a conference chat room](screenshot2.png)

## Credits

The userstyle is inspired by [Arc Dark](https://github.com/horst3180/Arc-theme), although it doesn't follow all its design choices.

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
